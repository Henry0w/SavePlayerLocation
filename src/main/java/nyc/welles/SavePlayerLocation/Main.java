package nyc.welles.SavePlayerLocation;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class Main extends JavaPlugin {
    private File customConfigFile;
    private FileConfiguration customConfig;
    public void saveCustomConfig() {
        try{
            customConfig.save(customConfigFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onEnable() {
        getConfig();
        saveConfig();
        createCustomConfig();
        for (World world : Bukkit.getWorlds()) {
            System.out.println("adding default: " + world.getName());
            getConfig().addDefault("worlds." + world.getName() + ".enabled", true);
            getConfig().addDefault("worlds." + world.getName() + ".group", "");
        }
       getConfig().options().copyDefaults(true);
        getServer().getPluginManager().registerEvents(new Listeners(this), this);
        saveConfig();
    }

    public FileConfiguration getCustomConfig() {
        return this.customConfig;
    }

    private void createCustomConfig() {
        customConfigFile = new File(getDataFolder(), "data.yml");
        if (!customConfigFile.exists()) {
            customConfigFile.getParentFile().mkdirs();
            saveResource("data.yml", false);
        }

        customConfig= new YamlConfiguration();
        try {
            customConfig.load(customConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

}
