package nyc.welles.SavePlayerLocation;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class Listeners implements Listener {
    private File customConfigFile;
    //    private JavaPlugin javaPlugin;
    private Main Main;
    private FileConfiguration customConfig;

    public Listeners(Main main) {
        this.Main = main;
    }

    //    private FileConfiguration config = this.javaPlugin.getConfig();
    public void loadCustomConfig() {
        customConfig = this.Main.getCustomConfig();
    }

    public void saveCustomConfig() {
        this.Main.saveCustomConfig();
    }

    public void savePlayerLocation(Player player, World world) {
        int x = player.getLocation().getBlockX();
        int y = player.getLocation().getBlockY();
        int z = player.getLocation().getBlockZ();
        System.out.println(" x: " + x + " y: " + y + " z: " + z + " World: " + world.getName() + " UUID: " + player.getUniqueId());
        customConfig.set("players." + player.getUniqueId() + ".coords." + world.getName() + ".x", x);
        customConfig.set("players." + player.getUniqueId() + ".coords." + world.getName() + ".y", y);
        customConfig.set("players." + player.getUniqueId() + ".coords." + world.getName() + ".z", z);
        customConfig.set("players." + player.getUniqueId() + ".coords." + world.getName() + ".lastUsed", true);
        for (World worldToTest : Bukkit.getWorlds()) {
            // tests that only worlds in same group are disabled
            if (this.Main.getConfig().getString("worlds." + worldToTest + ".group") != null && (this.Main.getConfig().getString("worlds." + world.getName() + ".group")) != null) {
                if (this.Main.getConfig().getString("worlds." + worldToTest + ".group").equals(this.Main.getConfig().getString("worlds." + world.getName() + ".group"))) {
                    // checks to see that worldToTest is not the same as player.getWorld() -- a little janky
                    if (!customConfig.getString("players." + player.getUniqueId() + ".coords.").equals(world.getName())) {
                        //disables world
                        customConfig.set("players." + player.getUniqueId() + ".coords." + worldToTest.getName() + ".lastUsed", false);
                    }
                }
            }


        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent playerQuitEvent) {
        Player player = playerQuitEvent.getPlayer();
        if (this.Main.getConfig().getBoolean("worlds." + player.getWorld().getName() + ".enabled")) {
            loadCustomConfig();
            savePlayerLocation(player, player.getWorld());
            saveCustomConfig();
        }
    }

    @EventHandler
    public void onPlayerEnterWorld(PlayerChangedWorldEvent playerChangedWorldEvent) {
        Player player = playerChangedWorldEvent.getPlayer();
        /*World previousWorld = playerChangedWorldEvent.getFrom();
        loadCustomConfig();
        savePlayerLocation(player,  previousWorld);
        saveCustomConfig();*/
        String playerWorldGroup = this.Main.getConfig().getString("worlds." + player.getWorld().getName() + ".group");
        for (World testWorld : Bukkit.getWorlds()) {
            System.out.println("inside for loop: testWorld is " + testWorld.getName());
            String testWorldGroup = this.Main.getConfig().getString("worlds." + testWorld.getName() + ".group");
            if (testWorldGroup == playerWorldGroup) {
                System.out.println("testWorldGroup, " + testWorldGroup + ", is equal to playerWorldGroup, " + playerWorldGroup);
                if (this.Main.getConfig().getBoolean("worlds." + testWorld.getName() + ".enabled")) {
                    System.out.println("tesWorld is enabled");
                    if (customConfig.getBoolean("players." + player.getUniqueId() + ".coords." + testWorld.getName() + ".lastUsed")) {
                        System.out.println("testWorld was last used, teleporting player");
                        int x = customConfig.getInt("players." + player.getUniqueId() + ".coords." + testWorld.getName() + ".x");
                        int y = customConfig.getInt("players." + player.getUniqueId() + ".coords." + testWorld.getName() + ".y");
                        int z = customConfig.getInt("players." + player.getUniqueId() + ".coords." + testWorld.getName() + ".z");
                        Location location = new Location(testWorld, x, y, z);
                        player.teleport(location);
                    }
                }

            }
        }
        /*int x = customConfig.getInt("players." + player.getUniqueId() + ".coords" + player.getWorld().getName() + ".x");
        int y = customConfig.getInt("players." + player.getUniqueId() + ".coords" + player.getWorld().getName() + ".y");
        int z = customConfig.getInt("players." + player.getUniqueId() + ".coords" + player.getWorld().getName() + ".z");
        Location location = new Location(player.getWorld(), x, y, z);
        player.teleport(location);*/
    }
}
